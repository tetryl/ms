-- Shows the amount of I/O performed by each database in the last 5 minutes.
-- No adjustments need to be made.
DECLARE @Sample TABLE (
	DB varchar(128) 
	,NumberOfReads bigint
	,NumberOfWrites bigint)

INSERT INTO @Sample 
SELECT name
	AS 'DB',
	
	SUM(num_of_reads)
	AS 'NumberOfRead',
	
	SUM(num_of_writes)
	AS 'NumberOfWrites'
	
FROM sys.dm_io_virtual_file_stats(NULL, NULL) I
	INNER JOIN sys.databases D  
	ON I.database_id = d.database_id
	GROUP BY name 

-- Definition of the measurement period
WAITFOR DELAY '00:05:00.000';

SELECT FirstSample.DB,
	(SecondSample.NumberOfReads - FirstSample.NumberOfReads)
	AS 'Number of Reads',
	
	(SecondSample.NumberOfWrites - FirstSample.NumberOfWrites)
	AS 'Number of Writes'
	
FROM 
	(SELECT * FROM @Sample) FirstSample
	
	INNER JOIN
	(SELECT name AS DB,
		SUM(num_of_reads)
		AS 'NumberOfReads',
		
		SUM(num_of_writes)
		AS 'NumberOfWrites'
		
	FROM sys.dm_io_virtual_file_stats(NULL, NULL) I
		INNER JOIN sys.databases D  
		ON I.database_id = d.database_id
		GROUP BY name)
		AS SecondSample
		
	ON FirstSample.DB = SecondSample.DB
	ORDER BY 'Number of Reads' DESC;