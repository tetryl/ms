-- Repair whole database.
-- Replace the value DBNAME with the name of the corresponding database.
DBCC UPDATEUSAGE ('DBNAME') with no_infomsgs;
GO