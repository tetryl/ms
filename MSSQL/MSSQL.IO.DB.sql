-- Shows the total I/O for each database.
-- No adjustments need to be made.
SELECT name AS DB,
	SUM(num_of_reads)
	AS 'Number of Read',
	
	SUM(num_of_writes)
	AS 'Number of Writes'

FROM sys.dm_io_virtual_file_stats(NULL, NULL) I
	INNER JOIN sys.databases D  
	ON I.database_id = d.database_id
	GROUP BY name
	ORDER BY 'Number of Read' DESC;