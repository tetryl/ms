-- Shrink LOG file.
-- !!!Be patient
-- Replace the value DBNAME with the name of the corresponding database.
USE DBNAME;
GO

-- Truncate the log by changing the database recovery model to SIMPLE.
ALTER DATABASE DBNAME
SET RECOVERY SIMPLE;
GO

-- Shrink the truncated log file to 1 MB.
-- !!!Only LDF files!!!
DBCC SHRINKFILE (DBNAME_Log, 1);
GO

-- Reset the database recovery model.
ALTER DATABASE DBNAME 
SET RECOVERY FULL;
GO
