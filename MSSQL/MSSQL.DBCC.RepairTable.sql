-- Repair a table in the database.
-- Replace the value DBNAME and TABLENAME with the name of the corresponding database and table.
DBCC UPDATEUSAGE ('DBNAME','TABLENAME') with no_infomsgs;
GO