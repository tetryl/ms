-- Check database health.
-- Replace the value DBNAME with the name of the corresponding database.
DBCC CHECKDB('DBNAME') with no_infomsgs;
GO