# MSSQL #

## Description ##
* DBCC: Check and repair scripts
* IO: Finding bottlenecks

## Prerequisites ##
* MSSQL

## Installing ##
* Copy file to a folder and run on SQL srv

### Files ###
* MSSQL.DBCC.CheckDB.sql - Check database health
* MSSQL.DBCC.RepairDB.sql - Repair database
* MSSQL.DBCC.RepairTable.sql - Repair a table in a database
* MSSQL.DBCC.ShrinkLog.sql - Shrink LOG
* MSSQL.IO.DB.sql - Shows the total I/O for each database
* MSSQL.IO.DB5min.sql - Shows the amount of I/O performed by each database in the last 5 minutes
* MSSQL.IO.HDDLatency.sql - Calculated disk latency for the different database drives
* MSSQL.IO.HDDStatistics.sql - Displaying I/O statistics by physical drive letter
* MSSQL.IO.Top25.sql - Display the top 25 most expensive read I/O queries
